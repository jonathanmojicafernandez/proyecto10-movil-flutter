import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/authentication/bloc.dart';
import 'package:proyecto10/bloc/navigator/bloc.dart';
import 'package:proyecto10/bloc/navigator/navigator.dart';
import 'package:proyecto10/bloc/prestamo/bloc.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/screens/detalle_screen.dart';
import 'package:proyecto10/ui/screens/error_widget.dart';
import 'package:proyecto10/ui/screens/home_screen.dart';
import 'package:proyecto10/ui/screens/inicio_screen.dart';
import 'package:proyecto10/ui/screens/loading_screen.dart';
import 'package:proyecto10/ui/screens/login_screen.dart';
import 'package:proyecto10/ui/screens/reigstro_screen.dart';
import 'package:proyecto10/ui/widgets/detalle_widget.dart';
import 'package:proyecto10/ui/widgets/inicio_widget.dart';
import 'package:proyecto10/utils/colores.dart';

import 'bloc/prestamo/event.dart';
import 'bloc/simple_bloc_delegate.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  var s = SimpleBlocDelegate();
  final Repository repository = Repository();
  runApp(MyApp(repository: repository));
}

class MyApp extends StatelessWidget {
  final Repository repository;
  const MyApp({Key? key, required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [


        BlocProvider(create: (context) {
          return AuthenticationBloc(repository: repository)..add(AppStart());
        }),
        BlocProvider(create: (context) {return PrestamoBloc(repository: repository, authenticationBloc: BlocProvider.of(context));
        }),
        BlocProvider(create: (context) {
          return NavigatorBloc(repository: repository,authenticationBloc: BlocProvider.of(context));
        }),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Prestamos",
        theme: ThemeData(
          primaryColor: Colors.black,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is AuthenticationSignUp) {
              return RegistroScreen(
                repository: repository,
              );
            }
            if (state is AuthenticationUnauthenticated) {
              return HomeScreen(repository: repository);
            }
            if (state is AuthenticationLogin) {
              return LoginScreen(repository: repository);
            }
            if (state is AuthenticationAuthenticated) {
              return App(repository: repository);
            }
            if (state is AuthenticationUnitialized) {
              return const SplashScreen();
            }
            return const SplashScreen();
          },
        ),
      ),
    );
  }
}

class App extends StatefulWidget {
  final Repository repository;
  const App({Key? key,required this.repository}) : super(key: key);

  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigatorBloc, NavigatorxState>(
        builder: (context, state) {
          return BlocBuilder<NavigatorBloc, NavigatorxState>(
              builder: (context, state) {
                if (state is StateInicio) {
                  return InicioScreen(repository: widget.repository);
                }
                if(state is StateInit)
                {
                  BlocProvider.of<NavigatorBloc>(context).add(GoInicio());
                }
                if(state is StateError)
                {
                  return ErrorScreen();
                }
                if (state is StateDetalle) {
                  return DetalleScreen(prestamo: state.prestamo);
                }
                return const SplashScreen();
              });
        });
  }

}
