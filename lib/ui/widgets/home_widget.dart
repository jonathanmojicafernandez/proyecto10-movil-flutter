import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/utils/colores.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key? key}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  double alto = 150;
  double altoPreparado = 0;
  Timer? timer;
  @override
  void initState() {
    super.initState();
    Timer.periodic(const Duration(seconds: 1), (timerr) {
      timer = timerr;
      if (alto == 150) {
        setState(() {
          alto = altoPreparado / 2 + 40;
          timerr.cancel();
        });
      }
      timerr.cancel();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer!.cancel();
  }

  @override
  Widget build(BuildContext context) {
    altoPreparado = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Container(
          margin: const EdgeInsets.only(left: 20, right: 20),
          child: Stack(
            children: [
              Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Image.asset("images/imagen.jpg")),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: AnimatedContainer(
                  duration: const Duration(seconds: 2),
                  height: alto,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                    color: Colores.azul,
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          margin: const EdgeInsets.only(
                              top: 30, bottom: 10, left: 10, right: 10),
                          child: Text("Sistema de prestamos de materiales",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.lato(
                                  fontSize: 19,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      BlocProvider.of<AuthenticationBloc>(
                                              context)
                                          .add(SignUpTap());
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.only(
                                          left: 30,
                                          right: 30,
                                          top: 10,
                                          bottom: 10),
                                      decoration: BoxDecoration(
                                          color: Colores.amarrillo,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      margin: const EdgeInsets.only(left: 20),
                                      child: Text(
                                        "Registro",
                                        style: GoogleFonts.lato(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      BlocProvider.of<AuthenticationBloc>(
                                              context)
                                          .add(LoginTap());
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.only(
                                          left: 30,
                                          right: 30,
                                          top: 10,
                                          bottom: 10),
                                      decoration: BoxDecoration(
                                          color: Colores.amarrillo,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      margin: const EdgeInsets.only(right: 20),
                                      child: Text(
                                        "Iniciar sesion",
                                        style: GoogleFonts.lato(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text(
                                    "Esta aplicacion unicamente es para usuarios de la universidad",
                                    style: GoogleFonts.lato(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w300),
                                  ))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
