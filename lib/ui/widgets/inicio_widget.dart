import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/navigator/bloc.dart';
import 'package:proyecto10/bloc/navigator/event.dart';
import 'package:proyecto10/bloc/prestamo/prestamo.dart';
import 'package:proyecto10/models/prestamo.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/screens/loading_screen.dart';
import 'package:proyecto10/utils/colores.dart';

class InicioWidget extends StatefulWidget {
  const InicioWidget({Key? key}) : super(key: key);

  @override
  _InicioWidgetState createState() =>
      _InicioWidgetState();
}

class _InicioWidgetState extends State<InicioWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PrestamoBloc,PrestamoState>(
      builder: (context,state) {
        if(state is PrestamoSuccessHistorial)
        {
          List<Prestamo> prestamos = state.prestamos;
          return RefreshIndicator(
            onRefresh: ()async{
              BlocProvider.of<PrestamoBloc>(context).add(PrestamoGet());
            },
            child: Container(
              margin: const EdgeInsets.only(left: 20, right: 20),
              child: ListView(
                children:  [
                  Container(
                    margin: EdgeInsets.only(top: 20,bottom: 20),
                    alignment: Alignment.center,
                    child: Text("Mis prestamos",style: GoogleFonts.lato(
                        fontSize: 24,
                        color: Colors.black,
                        fontWeight: FontWeight.bold
                    ),),
                  ),
                  prestamos.length > 0 ? Container(
                    child: Column(
                      children: List.generate(prestamos.length, (index){
                        return item(prestamos[index]);
                      })
                    ),
                  ) : Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Icon(Icons.warning,size: 200,color: Colors.yellow.shade900,),
                         Text("No se tiene ningun prestamo realizado",style: GoogleFonts.lato(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.w500
                        )),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      BlocProvider.of<AuthenticationBloc>(context).add(LogOut());
                    },
                    child: Container(
                      padding: const EdgeInsets.only(
                          left: 30, right: 30, top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: Colores.amarrillo,
                          borderRadius: BorderRadius.circular(10)),
                      margin: const EdgeInsets.only(top: 20),
                      child: Text(
                        "Cerrar sesión",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        return SplashScreen();

      }
    );
  }

  Widget item(Prestamo prestamo){
    return Container(
      child: GestureDetector(
        onTap: (){
          BlocProvider.of<NavigatorBloc>(context).add(GoDetalle(prestamo: prestamo));
        },
        child: Container(

          margin: const EdgeInsets.only(bottom: 10,top: 10),
          padding: const EdgeInsets.only(bottom: 30,top: 30),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: const <BoxShadow>[
                BoxShadow(
                    color: Colors.black54,
                    blurRadius: 2.0,
                    offset: Offset(1, 1)
                )
              ],
            borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colores.amarrillo)
            //color: prestamo.estatus! == "Prestado" ? Colores.coral :Colores.azul
          ),
          child: Container(
            margin: EdgeInsets.only(left: 20,right: 20),
            child: Column(
              children: [
                Container(

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Folio: "+prestamo.id.toString(),style: GoogleFonts.lato(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w500
                      ),),
                      Text("Estatus: "+prestamo.estatus.toString(),style: GoogleFonts.lato(
                          fontSize: 12,
                          color: Colores.azul,
                          fontWeight: FontWeight.bold
                      )),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text("Fecha inicio: "+prestamo.inicio.toString(),style: GoogleFonts.lato(
                      fontSize: 10,
                      color: Colors.black,
                      fontWeight: FontWeight.w500
                  )),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text("Fecha fin: "+prestamo.fin.toString(),style: GoogleFonts.lato(
                      fontSize: 10,
                      color: Colors.black,
                      fontWeight: FontWeight.w500
                  )),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text("Cantidad de materiales: "+prestamo.entradaprestamos!.length.toString(),style: GoogleFonts.lato(
                      fontSize: 10,
                      color: Colors.black,
                      fontWeight: FontWeight.w500
                  )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
