import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proyecto10/bloc/navigator/bloc.dart';
import 'package:proyecto10/bloc/navigator/event.dart';
import 'package:proyecto10/models/entradas_prestamo.dart';
import 'package:proyecto10/models/prestamo.dart';
import 'package:proyecto10/utils/colores.dart';

class DetalleWidget extends StatefulWidget {
  final Prestamo prestamo;
  DetalleWidget({Key? key, required this.prestamo}) : super(key: key);

  @override
  _DetalleWidgetState createState() => _DetalleWidgetState();
}

class _DetalleWidgetState extends State<DetalleWidget> {
  @override
  Widget build(BuildContext context) {
    Prestamo prestamo = widget.prestamo;
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: ListView(
        children: [
          Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                    icon: const Icon(Icons.arrow_back_ios_outlined, size: 20),
                    onPressed: () {
                      BlocProvider.of<NavigatorBloc>(context).add(GoInicio());
                    }),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Text(prestamo.estatus!,
                      style: GoogleFonts.lato(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
              )
            ],
          ),
          Container(
              child: Column(
            children: [
              Text("Folio: " + prestamo.id!.toString(),style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500)),
              Text("Fecha de prestamo: " + prestamo.inicio!.toString(),style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500)),
              Text("Fecha fin: " + prestamo.fin!.toString(),style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500)),
              Text("Cantidad de materiales: " + prestamo.entradaprestamos!.length.toString(),style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500)),
              Text("Laboratorio: " + prestamo.entradaprestamos!.first.laboratorio!.nombre!,style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500)),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Wrap(
                  children: List.generate(prestamo.entradaprestamos!.length, (index){
                    return Item(prestamo.entradaprestamos![index]);
                  })
                ),
              )
            ],
          )),
        ],
      ),
    );
  }

  Widget Item(EntradasPrestamo e)
  {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Image.network(e.material!.foto!,height: 100,width: 100),
          Container(
            width: 140,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Clave: "+e.clave.toString()),
                Text("Nombre: "+e.material!.nombre!),
                Text("Categoria: "+e.material!.categoria!)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
