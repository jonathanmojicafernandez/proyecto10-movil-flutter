import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/navigator/navigator.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {

  AppBarWidget({Key? key}) : super(key: key);
  @override
  Size get preferredSize => Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;
    return AppBar(
      automaticallyImplyLeading: false,
      actions: <Widget>[Container()],
      elevation: .5,
      backgroundColor: Colors.white,
      flexibleSpace: Container(
        padding: EdgeInsets.only(top: statusbarHeight + 10, bottom: 10, left: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                BlocProvider.of<NavigatorBloc>(context).add(GoInicio());
              },
              child:Container(
                  child: Image(
                    image: AssetImage('images/logo.png'),
                    width: 100,
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
