import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proyecto10/bloc/navigator/bloc.dart';
import 'package:proyecto10/bloc/navigator/navigator.dart';

class ErrorWidget extends StatefulWidget {
  const ErrorWidget({Key? key}) : super(key: key);

  @override
  _ErrorWidgetState createState() => _ErrorWidgetState();
}

class _ErrorWidgetState extends State<ErrorWidget> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: ()async{
        BlocProvider.of<NavigatorBloc>(context).add(GoInit());
      },
      child: Container(
        margin: EdgeInsets.only(left: 20,right: 20),
        child: ListView(
          children: <Widget>[
            Container(
              child: Icon(Icons.error,color: Colors.red,size: 200,),
            ),
            Container(
              alignment: Alignment.center,
              child: Text("No has sido aceptado por algun laboratorista o administrador",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.w500
              )),
            ),
          ],
        ),
      ),
    );
  }
}
