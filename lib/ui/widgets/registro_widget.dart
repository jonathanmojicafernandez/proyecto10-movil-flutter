import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/navigator/bloc.dart';
import 'package:proyecto10/bloc/registro/registro.dart';
import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/utils/colores.dart';

class RegistroWidget extends StatefulWidget {
  RegistroWidget({Key? key}) : super(key: key);

  @override
  _RegistroWidgetState createState() => _RegistroWidgetState();
}

class _RegistroWidgetState extends State<RegistroWidget> {
  TextEditingController _matricula = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _nombre = TextEditingController();
  TextEditingController _apellidoP = TextEditingController();
  TextEditingController _apellidoM = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocListener<RegistroBloc, RegistroState>(
      listener: (context, state) {
        if (state is RegistroError) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.error),
            backgroundColor: Colors.red,
          ));
        }
      },
      child: SafeArea(
        child: Container(
            margin: const EdgeInsets.only(left: 20, right: 20),
            child: Stack(
              children: [
                Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Image.asset("images/logo.png",height: 200,width: 100,)),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      color: Colores.azul,
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              top: 30, bottom: 10, left: 10, right: 10),
                          child: Text("Registro",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.lato(
                                  fontSize: 19,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          height: 200,
                          child: ListView(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: TextField(
                                  controller: _nombre,
                                  autocorrect: false,
                                  decoration: const InputDecoration(
                                      hintText: "Nombre",
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colores.coral)),
                                      hintStyle: TextStyle(color: Colors.grey)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: TextField(
                                  controller: _apellidoP,
                                  autocorrect: false,
                                  decoration: const InputDecoration(
                                      hintText: "Apellido Paterno",
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colores.coral)),
                                      hintStyle: TextStyle(color: Colors.grey)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: TextField(
                                  controller: _apellidoM,
                                  autocorrect: false,
                                  decoration: const InputDecoration(
                                      hintText: "Apellido Materno",
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colores.coral)),
                                      hintStyle: TextStyle(color: Colors.grey)),
                                ),
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.only(left: 20, right: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: TextField(
                                  controller: _matricula,
                                  autocorrect: false,
                                  decoration: const InputDecoration(
                                      hintText: "Matricula",
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colores.coral)),
                                      hintStyle: TextStyle(color: Colors.grey)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 20, right: 20, top: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: TextField(
                                  obscureText: true,
                                  controller: _password,
                                  autocorrect: false,
                                  decoration: const InputDecoration(
                                      hintText: "Password",
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colores.coral)),
                                      hintStyle: TextStyle(color: Colors.grey)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            if (_nombre.text.isNotEmpty ||
                                _password.text.isNotEmpty ||
                                _nombre.text.isNotEmpty ||
                                _apellidoP.text.isNotEmpty ||
                                _apellidoM.text.isNotEmpty) {
                              BlocProvider.of<RegistroBloc>(context).add(
                                  RegistroBtnTap(
                                      nombre: _nombre.text,
                                      matricula: _matricula.text,
                                      password: _password.text,
                                      apellidoP: _apellidoP.text,
                                      apellidoM: _apellidoM.text));
                            } else {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                content: Text("Debes llenar todos los campos"),
                                backgroundColor: Colors.red,
                              ));
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.only(
                                left: 30, right: 30, top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: Colores.amarrillo,
                                borderRadius: BorderRadius.circular(10)),
                            margin: const EdgeInsets.only(top: 60),
                            child: Text(
                              "Registrarme",
                              style: GoogleFonts.lato(
                                  fontSize: 15,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            BlocProvider.of<AuthenticationBloc>(context)
                                .add(LogOut());
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 20, bottom: 10),
                            child: Text(
                              "Regresar al menu",
                              style: GoogleFonts.lato(
                                  fontSize: 15,
                                  color: Colors.white,
                                  decoration: TextDecoration.underline,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
