import 'package:flutter/material.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/widgets/home_widget.dart';

class HomeScreen extends StatelessWidget {
  final Repository repository;
  const HomeScreen({Key? key, required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: HomeWidget(),
    );
  }
}
