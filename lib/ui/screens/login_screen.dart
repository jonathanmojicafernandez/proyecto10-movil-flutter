import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/login/login.dart';
import 'package:proyecto10/bloc/registro/registro.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/widgets/login_widget.dart';

class LoginScreen extends StatelessWidget {
  final Repository repository;
  const LoginScreen({Key? key, required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
          create: (context) {
            return LoginBloc(
                repository: repository,
                authenticationBloc:
                    BlocProvider.of<AuthenticationBloc>(context));
          },
          child: LoginWidget()),
    );
  }
}
