import 'package:flutter/material.dart';
import 'package:proyecto10/ui/widgets/app_bar_widget.dart';
import 'package:proyecto10/ui/widgets/error_widget.dart' as x;

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBarWidget(),
        body: x.ErrorWidget(),
      ),
    );
  }
}
