import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/prestamo/bloc.dart';
import 'package:proyecto10/bloc/prestamo/prestamo.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/widgets/app_bar_widget.dart';
import 'package:proyecto10/ui/widgets/inicio_widget.dart';

import 'loading_screen.dart';

class InicioScreen extends StatelessWidget{
  final Repository repository;
  const InicioScreen({Key? key,required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context){
          return PrestamoBloc(repository: repository, authenticationBloc: BlocProvider.of(context))..add(PrestamoGet());
    }
        )],
      child: SafeArea(
        child: Scaffold(
          appBar: AppBarWidget(),
          body: InicioWidget(),
        ),
      ),
    );
  }
}