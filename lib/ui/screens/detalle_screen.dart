import 'package:flutter/material.dart';
import 'package:proyecto10/models/prestamo.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/widgets/app_bar_widget.dart';
import 'package:proyecto10/ui/widgets/detalle_widget.dart';

class DetalleScreen extends StatelessWidget {
  final Prestamo prestamo;
  const DetalleScreen({Key? key,required this.prestamo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: DetalleWidget(prestamo: prestamo),
    );
  }
}