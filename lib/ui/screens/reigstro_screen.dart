import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/registro/registro.dart';
import 'package:proyecto10/resources/repository.dart';
import 'package:proyecto10/ui/widgets/registro_widget.dart';

class RegistroScreen extends StatelessWidget {
  final Repository repository;
  const RegistroScreen({Key? key, required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
          create: (context) {
            return RegistroBloc(
                repository: repository,
                authenticationBloc:
                    BlocProvider.of<AuthenticationBloc>(context));
          },
          child: RegistroWidget()),
    );
  }
}
