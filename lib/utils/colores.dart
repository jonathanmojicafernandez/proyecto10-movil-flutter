import 'package:flutter/material.dart';

class Colores {
  static const Color turquesa = Color(0xFF37CFD2);
  static const Color azul = Color(0xFF553DFB);
  static const Color gris = Color(0xFFC8C9E1);
  static const Color amarrillo = Color(0xFFFFCE40);
  static const Color coral = Color(0xFFFE8180);
}
