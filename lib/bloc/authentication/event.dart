import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/user.dart';

class AuthenticationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AppStart extends AuthenticationEvent {}

class SignUpTap extends AuthenticationEvent {}

class LoginTap extends AuthenticationEvent {}

class LogOut extends AuthenticationEvent {}

class Login extends AuthenticationEvent {
  final User user;
  Login({required this.user});

  @override
  List<Object> get props => [user];

  @override
  String toString() => 'Login: {${user.nombre}}';
}
