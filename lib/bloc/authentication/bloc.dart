import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/resources/repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {

  final Repository repository;
  AuthenticationBloc({required this.repository})
      : super(AuthenticationUnitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppStart) {
      final bool hasToken = await repository.dbHasToken();
      if (hasToken) {
        yield AuthenticationAuthenticated();
      } else {
        yield AuthenticationUnauthenticated();
      }
    }
    if (event is Login) {
      yield AuthenticationLoading();
      await repository.dbDeleteUsers();
      await repository.dbSaveUser(event.user);
      yield AuthenticationAuthenticated();
    }
    if (event is SignUpTap) {
      yield AuthenticationSignUp();
    }
    if (event is LoginTap) {
      yield AuthenticationLogin();
    }
    if (event is LogOut) {
      yield AuthenticationLoading();
      await repository.dbDeleteUsers();
      yield AuthenticationUnauthenticated();
    }
  }
}
