import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/prestamo/prestamo.dart';
import 'package:proyecto10/models/prestamo.dart';
import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/resources/repository.dart';

class PrestamoBloc extends Bloc<PrestamoEvent, PrestamoState> {
  final AuthenticationBloc authenticationBloc;
  final Repository repository;
  PrestamoBloc({required this.repository,required this.authenticationBloc})
      : super(PrestamoLoading());

  @override
  Stream<PrestamoState> mapEventToState(
      PrestamoEvent event) async* {
        User user = await repository.dbGetUser();
        if (event is PrestamoGet) {

          List<Prestamo> prestamos =await repository.apiPrestamos(user.token!);
          yield PrestamoSuccessHistorial(prestamos: prestamos);
        }
  }
}
