import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/user.dart';

class PrestamoEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PrestamoGet extends PrestamoEvent {}
