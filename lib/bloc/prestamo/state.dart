import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/prestamo.dart';

abstract class PrestamoState extends Equatable {
  @override
  List<Object> get props => [];
}

class PrestamoSuccessHistorial extends PrestamoState {
  final List<Prestamo> prestamos;

  PrestamoSuccessHistorial({required this.prestamos});

  @override
  List<Object> get props => [prestamos];

  @override
  String toString() => "PrestamoSuccessHistorial {error $prestamos}";
}

class PrestamoLoading extends PrestamoState {}

class PrestamoError extends PrestamoState {
  final String error;

  PrestamoError({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => "Error al hacer Prestamo {error $error}";
}
