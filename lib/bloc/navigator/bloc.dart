import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/navigator/navigator.dart';
import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/resources/repository.dart';

class NavigatorBloc extends Bloc<NavigatorxEvent, NavigatorxState> {
  Repository repository;
  AuthenticationBloc authenticationBloc;
  NavigatorBloc({required this.repository,required this.authenticationBloc}) : super(StateInit());

  @override
  Stream<NavigatorxState> mapEventToState(NavigatorxEvent event) async* {
    if (event is GoInicio) {
      try{
        User usuariodb = await repository.dbGetUser();
        User user = await repository.apiGetUser(usuariodb.token!);
        print(user.estatus);
        if(user.estatus == "Aceptado")
        {
          yield StateInicio();
        }else{
          yield StateError();
        }
      }catch(e){
        authenticationBloc.add(LogOut());
      }


    }
    if(event is GoInit)
    {
      yield StateInit();
    }
    if (event is GoDetalle) {
      yield StateDetalle(prestamo: event.prestamo);
    }
  }
}
