import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/prestamo.dart';

abstract class NavigatorxState extends Equatable {
  @override
  List<Object> get props => [];
}

class StateInicio extends NavigatorxState {}
class StateInit extends NavigatorxState {}
class StateError extends NavigatorxState {}

class StateDetalle extends NavigatorxState {
  final Prestamo prestamo;

  StateDetalle({required this.prestamo});

  @override
  List<Object> get props => [prestamo];

  @override
  String toString() => "Error al hacer Login {error $prestamo}";
}
