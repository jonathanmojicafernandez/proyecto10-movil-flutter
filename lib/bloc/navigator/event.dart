import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/prestamo.dart';

abstract class NavigatorxEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GoInicio extends NavigatorxEvent {}
class GoInit extends NavigatorxEvent {}

class GoDetalle extends NavigatorxEvent {
  final Prestamo prestamo;

  GoDetalle({required this.prestamo});

  @override
  List<Object> get props => [prestamo];

  @override
  String toString() => "GoDetalle {matricula: $prestamo}";
}
