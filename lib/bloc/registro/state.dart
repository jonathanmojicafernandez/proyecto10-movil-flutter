import 'package:equatable/equatable.dart';

abstract class RegistroState extends Equatable {
  @override
  List<Object> get props => [];
}

class RegistroInit extends RegistroState {}

class RegistroLoading extends RegistroState {}

class RegistroError extends RegistroState {
  final String error;

  RegistroError({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => "Error al hacer registro {error $error}";
}
