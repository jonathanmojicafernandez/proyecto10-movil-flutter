import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/registro/registro.dart';
import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/resources/repository.dart';

class RegistroBloc extends Bloc<RegistroEvent, RegistroState> {
  final Repository repository;
  final AuthenticationBloc authenticationBloc;

  RegistroBloc({required this.repository, required this.authenticationBloc})
      : super(RegistroLoading());

  @override
  Stream<RegistroState> mapEventToState(RegistroEvent event) async* {
    if (event is RegistroBtnTap) {
      yield RegistroLoading();
      try {
        final User user = await repository.apiRegistro(event.nombre,
            event.password, event.matricula, event.apellidoP, event.apellidoM);
        authenticationBloc.add(Login(user: user));
        yield RegistroInit();
      } catch (error) {
        yield RegistroError(error: error.toString().substring(11));
      }
    }
  }
}
