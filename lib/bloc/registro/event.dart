import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/user.dart';

class RegistroEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RegistroBtnTap extends RegistroEvent {
  final String matricula;
  final String password;
  final String nombre;
  final String apellidoP;
  final String apellidoM;

  RegistroBtnTap(
      {required this.nombre,
      required this.matricula,
      required this.password,
      required this.apellidoP,
      required this.apellidoM});

  @override
  List<Object> get props => [nombre, matricula, password, apellidoP, apellidoM];

  @override
  String toString() => "RegistroBtn {matricula: $matricula}";
}
