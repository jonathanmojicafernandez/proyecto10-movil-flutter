import 'package:equatable/equatable.dart';
import 'package:proyecto10/models/user.dart';

abstract class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginBtnTap extends LoginEvent {
  final String matricula;
  final String password;

  LoginBtnTap({required this.matricula, required this.password});

  @override
  List<Object> get props => [matricula, password];

  @override
  String toString() => "LoginBtn {matricula: $matricula}";
}
