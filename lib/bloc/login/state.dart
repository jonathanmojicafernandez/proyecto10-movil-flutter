import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginInit extends LoginState {}

class LoginLoading extends LoginState {}

class LoginError extends LoginState {
  final String error;

  LoginError({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => "Error al hacer Login {error $error}";
}
