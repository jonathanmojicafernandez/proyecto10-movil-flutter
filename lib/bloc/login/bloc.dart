import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto10/bloc/authentication/authentication.dart';
import 'package:proyecto10/bloc/login/login.dart';
import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/resources/repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final Repository repository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({required this.repository, required this.authenticationBloc})
      : super(LoginLoading());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginBtnTap) {
      yield LoginLoading();
      try {
        final User user =
            await repository.apiLogin(event.matricula, event.password);
        authenticationBloc.add(Login(user: user));
        yield LoginInit();
      } catch (error) {
        print(error);
        yield LoginError(error: error.toString());
      }
    }
  }
}
