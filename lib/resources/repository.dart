import 'package:proyecto10/models/user.dart';
import 'package:proyecto10/resources/api_provider.dart';
import 'package:proyecto10/resources/sqlite_provider.dart';

class Repository {
  final _dbProvider = SqliteProvider();
  final _apiProvider = ApiProvider();

  Future apiRegistro(String nombre, String password, String matricula,
          String apellidoP, String apellidoM) =>
      _apiProvider.registrar(nombre, password, matricula, apellidoP, apellidoM);

  Future apiLogin(String matricula, String password) =>
      _apiProvider.login(matricula, password);

  Future apiPrestamos(String token) => _apiProvider.getPrestamos(token);
  Future apiGetUser(String token) => _apiProvider.getEstatusUser(token);

  Future<void> dbDeleteUsers() => _dbProvider.deleteUsers();
  Future<void> dbSaveUser(User user) => _dbProvider.saveUser(user);
  Future dbGetUser() => _dbProvider.getUser();
  Future dbHasToken() => _dbProvider.hasToken();
}
