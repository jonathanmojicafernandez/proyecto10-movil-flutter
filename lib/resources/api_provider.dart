import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:proyecto10/models/prestamo.dart';
import 'package:proyecto10/models/user.dart';

class ApiProvider {
  final String ip = "https://secret-inlet-22565.herokuapp.com";
  Dio dio = Dio();
  Response? response;
  Options options = Options(
      method: 'POST', headers: {HttpHeaders.acceptHeader: "application/json"});

  Options optionsToken(String token, String tipo) {
    return Options(method: tipo, headers: {
      HttpHeaders.acceptCharsetHeader: "application/json",
      HttpHeaders.authorizationHeader: {"x-access-token": token}
    });
  }

  Future registrar(String nombre, String password, String matricula,
      String apellidoP, String apellidoM) async {
    try {
      var map =  Map<String, dynamic>();
      map['nombres'] = nombre;
      map['matricula'] = matricula;
      map['password'] = password;
      map['apellido1'] = apellidoP;
      map['apellido2'] = apellidoM;
      map['estatus'] = "Rechazado";
      response =
          await dio.request("$ip/movile/user/add", data: map, options: options);
      if (response!.statusCode == 200) {
        final data = response!.data;
        User user = User.fromJson(data);
        return user;
      }
    } on DioError catch (e) {
      if (e.response == null) {
        final data = e.response!.data;
        return throw Exception(data['response']);
      } else {
        return throw Exception(e.message);
      }
    }
  }

  Future login(String matricula, String password) async {
    try {
      var map = new Map<String, dynamic>();
      map['matricula'] = matricula;
      map['password'] = password;
      response = await dio.request("$ip/movile/user/login",
          data: map, options: options);
      if (response!.statusCode == 200) {
        final data = response!.data;
        User user = User.fromJson(data['user']);
        return user;
      }
    } on DioError catch (e) {
      if (e.response == null) {
        final data = e.response!.data;
        return throw Exception(data);
      } else {
        return throw e.response!.data['response'];
      }
    }
  }

  Future getPrestamos(String token) async {
    try {
      response = await dio.request("$ip/movil/consultar/prestamoHistorial",
          options: Options(
            headers: {
              "x-access-token": token
            }
          ));
      if (response!.statusCode == 200) {
        final data = response!.data;
        List<Prestamo> prestamos = List<Prestamo>.from(data.map((x) => Prestamo.fromJson(x)));
        return prestamos;
      }
    } on DioError catch (e) {
      log(e.message);
      if (e.response == null) {
        final data = e.response!.data;
        return throw Exception(data);
      } else {
        return throw Exception(e.message);
      }
    }
  }

  Future getEstatusUser(String token) async {
    try {
      response = await dio.request("$ip/movil/consultar/usuario",
          options: Options(
              headers: {
                "x-access-token": token
              }
          ));
      if (response!.statusCode == 200) {
        final data = response!.data;
        User user = User.fromJson(data);
        return user;
      }
    } on DioError catch (e) {
      log(e.message);
      if (e.response == null) {
        final data = e.response!.data;
        return throw Exception(data);
      } else {
        return throw e.message;
      }
    }
  }
}
