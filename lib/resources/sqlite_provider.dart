import 'package:proyecto10/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'dart:async';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class SqliteProvider {
  Database? _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    } else {
      _db = await initDb();
      return _db!;
    }
  }

  initDb() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "almacen_jona.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  closeDb(Database db) async {
    await db.close();
  }

  _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE user(id INTEGER PRIMARY KEY,nombre TEXT NOT NULL,token TEXT NOT NULL)");
  }

  Future<User> getUser() async {
    var dbClient = await db;
    var res = await dbClient.query("user");
    if (res.length > 0) {
      User user = User.fromJson(res.first);
      return user;
    }
    return User();
  }

  Future<bool> hasToken() async {
    var dbClient = await db;
    var res = await dbClient.query("user");
    if (res.length > 0) {
      return true;
    }
    return false;
  }

  Future<void> deleteUsers() async {
    var dbClient = await db;
    await dbClient.delete("user");
  }

  Future<void> saveUser(User user) async {
    var dbClient = await db;
    await dbClient.delete("user");
    await dbClient.insert("user", user.toMap());
  }
}
