class Material{
  final int? id;
  final String? nombre;
  final String? categoria;
  final String? foto;
  
  Material({
    required this.id,
    required this.nombre,
    required this.categoria,
    required this.foto
  });

  factory Material.fromJson(Map<String, dynamic> data) {
    return Material(
      id: data['_id'],
      nombre: data['nombre'],
      categoria: data['categoria']['nombre'],
      foto: data['foto']
    );
  }
}