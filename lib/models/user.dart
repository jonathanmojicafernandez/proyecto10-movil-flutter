class User {
  final String? nombre;
  final String? token;
  final String? estatus;

  User({this.nombre, this.token,this.estatus});

  factory User.fromJson(Map<String, dynamic> data) {
    return User(nombre: data['nombres'], token: data['token'],estatus: data['estatus']);
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{"nombre": nombre, "token": token};
  }
}
