class Laboratorio{
  final String? nombre;
  Laboratorio({required this.nombre});

  factory Laboratorio.fromJson(Map<String, dynamic> data) {
    return Laboratorio(
        nombre: data['nombre'],
    );
  }

}