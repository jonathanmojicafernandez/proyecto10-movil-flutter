import 'package:proyecto10/models/material.dart';

import 'laboratoio.dart';

class EntradasPrestamo{
  final int? id;
  final String? clave;
  final Laboratorio? laboratorio;
  final Material? material;

  EntradasPrestamo({
    required this.id,
    required this.clave,
    required this.laboratorio,
    required this.material
  });

  factory EntradasPrestamo.fromJson(Map<String, dynamic> data) {
    print(data);
    return EntradasPrestamo(
      id: data['_id'],
      clave: data['clave'],
      laboratorio: Laboratorio.fromJson(data['laboratorio']),
      material: Material.fromJson(data['material'])
    );
  }
}