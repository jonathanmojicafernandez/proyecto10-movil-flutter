import 'package:proyecto10/models/entradas_prestamo.dart';

class Prestamo{
  final int? id;
  final String? inicio;
  final String? fin;
  final String? estatus;
  final List<EntradasPrestamo>? entradaprestamos;

  Prestamo({
    required this.id,
    required this.inicio,
    required this.fin,
    required this.estatus,
    required this.entradaprestamos
  });
  factory Prestamo.fromJson(Map<String, dynamic> data) {
    return Prestamo(
      id: data['_id'],
      inicio: data['inicio'],
      fin: data['fin'],
      estatus: data['estatus'],
      entradaprestamos: List<EntradasPrestamo>.from(data['entradas_prestamo'].map((x) => EntradasPrestamo.fromJson(x)))
    );
  }
}